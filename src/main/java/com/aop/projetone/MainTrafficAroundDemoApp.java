package com.aop.projetone;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aop.projetone.service.TrafficFortuneService;

public class MainTrafficAroundDemoApp {

	public static void main(String[] args) {
		// read the spring config
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// call the service
		TrafficFortuneService trafficFortune = context.getBean("trafficFortuneService", TrafficFortuneService.class);
		
		System.out.println("Arround demo app");
		System.out.println("Call demo app");
		
		String data = trafficFortune.getFortune();
		System.out.println("My fortune + " + data);
		System.out.println("Finished");

		// close
		context.close();
	}

}
