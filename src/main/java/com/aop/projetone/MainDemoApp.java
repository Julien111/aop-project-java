package com.aop.projetone;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aop.projetone.dao.AccountDao;
import com.aop.projetone.dao.MembersDao;
import com.aop.projetone.model.Account;

public class MainDemoApp {

	public static void main(String[] args) {
		// read the spring config
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// get the bean
		AccountDao theAccountDao = context.getBean("accountDao", AccountDao.class);

		// call the business method
		//theAccountDao.addAccount();
		
		//test
		
		System.out.println("New test call");
		Account myAccount = new Account();		
		
		//test getter setter		
		myAccount.setName("Douglas");
		myAccount.setLevel("silver");
		theAccountDao.addAccount(myAccount, true);
		theAccountDao.doWork();
		
		String name = theAccountDao.getName();
		String code = theAccountDao.getServiceCode();
		
		//get member
		MembersDao memberDao = context.getBean("membersDao", MembersDao.class);
		//add member
		memberDao.addTest();
		
		//test member boolean 
		memberDao.goToAnotherStuff();
		memberDao.addMembersTest();

		// close
		context.close();
	}

}
