package com.aop.projetone.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.aop.projetone.model.Account;

@Component
public class AccountDao {
	
	private String name;
	
	private String serviceCode;	
	
	public String getName() {
		System.out.println(getClass() + ": in getName()");
		return name;
	}

	public void setName(String name) {
		System.out.println(getClass() + ": in setName()");
		this.name = name;
	}

	public String getServiceCode() {
		System.out.println(getClass() + ": in getServiceCode()");
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		System.out.println(getClass() + ": in setServiceCode()");
		this.serviceCode = serviceCode;
	}

	public void addAccount(Account account, boolean vipFlag) {
		System.out.println(account.getClass() + " : add an account !");
	}
	
	public boolean doWork() {
		System.out.println(getClass() + ": doWork()");
		return false;
	}
	
	/**
	 * Fonction pour la partie @AfterReturning
	 * @param tripWire 
	 * @return
	 */
	public List<Account> findAccounts(boolean tripWire){
		
		//for academic 
		if(tripWire) {
			throw new RuntimeException("<<<<<<<<<<< Error >>>>>>>>>>>");
		}
		
		
		List<Account> myAccounts = new ArrayList<>();
		
		Account acc1 = new Account("John", "Silver");
		Account acc2 = new Account("Dan", "Gold");
		Account acc3 = new Account("Jules", "Platinum");
		
		// add to the list
		myAccounts.add(acc1);
		myAccounts.add(acc2);
		myAccounts.add(acc3);
		
		//return list
		return myAccounts;
	}
}
