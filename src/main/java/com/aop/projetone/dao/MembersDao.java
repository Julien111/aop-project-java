package com.aop.projetone.dao;

import org.springframework.stereotype.Component;

@Component
public class MembersDao {
	
	public void addTest() {
		System.out.println(getClass() + " : add member account.");
	}
	
	public boolean addMembersTest() {
		System.out.println(getClass() + " : create new member !");
		return true;
	}
	
	public void goToAnotherStuff() {
		System.out.println(getClass() + " : another method test.");
	}
	
}
