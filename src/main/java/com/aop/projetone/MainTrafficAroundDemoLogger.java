package com.aop.projetone;


import java.util.logging.Logger;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aop.projetone.service.TrafficFortuneService;

public class MainTrafficAroundDemoLogger {
	
	private static Logger myLogger = Logger.getLogger(MainTrafficAroundDemoLogger.class.getName());

	public static void main(String[] args) {
		// read the spring config
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// call the service
		TrafficFortuneService trafficFortune = context.getBean("trafficFortuneService", TrafficFortuneService.class);
		
		myLogger.info("Arround demo app");
		myLogger.info("Call demo app");
		
		String data = trafficFortune.getFortune();
		
		myLogger.info("My fortune + " + data);
		myLogger.info("Finished");

		// close
		context.close();
	}
	
	

}
