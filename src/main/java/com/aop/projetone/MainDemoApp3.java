package com.aop.projetone;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aop.projetone.dao.AccountDao;
import com.aop.projetone.dao.MembersDao;
import com.aop.projetone.model.Account;

public class MainDemoApp3 {

	public static void main(String[] args) {
		// read the spring config
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);

		// get the bean
		AccountDao theAccountDao = context.getBean("accountDao", AccountDao.class);

		//call method
		List<Account> theAccounts = null;
		
		try {
			boolean tripWire = true;
			theAccounts = theAccountDao.findAccounts(tripWire);
		}catch(Exception e) {
			System.out.println("Program caught exception: " + e);
		}
		//display
		//System.out.println("After returning program");
		System.out.println("After throwing demo program");
		System.out.println(" <<<<TEST>>>>");		
		System.out.println(theAccounts);
		System.out.println("\n");

		// close
		context.close();
	}

}
