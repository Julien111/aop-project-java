package com.aop.projetone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class ProjetoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoneApplication.class, args);		
	}

}
