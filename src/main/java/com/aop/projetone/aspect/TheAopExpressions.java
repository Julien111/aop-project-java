package com.aop.projetone.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TheAopExpressions {
	
	@Pointcut("execution(* com.aop.projetone.dao.*.*(..))")
	public void forDaoPackage() {

	}

	// pointcut for getter

	@Pointcut("execution(* com.aop.projetone.dao.*.get*(..))")
	public void getter() {

	}

	// pointcut for setter

	@Pointcut("execution(* com.aop.projetone.dao.*.set*(..))")
	public void setter() {

	}

	// exclude getter and setter

	@Pointcut("forDaoPackage() && !(getter() || setter())")
	public void forDaoPackageNoGetterSetter() {

	}
}
