package com.aop.projetone.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.aop.projetone.model.Account;

@Aspect
@Component
@Order(2)
public class MyDemoLoggingAspect {
	
	private Logger myLogger = Logger.getLogger(getClass().getName());

	// content for logging
	// annotation before

	// @Before("execution(public void addAccount())")

//	@Before("execution(* com.aop.projetone.dao.*.*(..))")
//	public void beforeAddAccountAdvice() {
//		myLogger.info("\n ====>>>> Code first @before the account method");
//	}
	
	@Around("execution(* com.aop.projetone.service.TrafficFortuneService.getFortune(..))")
	public Object arroundGetFortune(ProceedingJoinPoint theProccedingJoinPoint) throws Throwable  {
		//print the method
		String method = theProccedingJoinPoint.getSignature().toShortString();
		myLogger.info("\n=======>>> Executing @Around on method: " + method);
		
		//begin timestamp
		long begin = System.currentTimeMillis();
		
		//object execute method
		Object result = null;
		try {
			result = theProccedingJoinPoint.proceed();
		} catch (Throwable e) {	
			myLogger.warning(e.getMessage());
			//result = "Major problem !!!!!!";
			throw e;
		}
		
		long end = System.currentTimeMillis();
		
		//calcul
		
		long duration = end - begin;
		
		myLogger.info("Afficher le temps : " + duration / 1000.0 + " seconds.");
		
		return result;
	}
	
	//add new advice
	@AfterReturning(pointcut="execution(* com.aop.projetone.dao.AccountDao.findAccounts(..))", returning="result")
	public void afterReturningFindAccountAdvice(JoinPoint joinPoint, List<Account> result) {
		String method = joinPoint.getSignature().toShortString();
		myLogger.info("\n===>>> Executing @AfterReturning on method: " + method);	
		
		//print the result
		myLogger.info("\n=====>>>> result is: " + result);
		
		//test on the list
		
		convertAccountNamesToUpperCase(result);
		
	}
	
	@AfterThrowing(pointcut="execution(* com.aop.projetone.dao.AccountDao.findAccounts(..))", throwing="e")
	public void afterThrowingFindAccountAdvice(JoinPoint theJoinPoint, Throwable e) {
		//print the method
		String method = theJoinPoint.getSignature().toShortString();
		myLogger.info("\n=======>>> Executing @AfterThrowing on method: " + method);
	}
	
	/**
	 * Advice for method after	
	 * @param theJoinPoint 
	 */
	@After("execution(* com.aop.projetone.dao.AccountDao.findAccounts(..))")
	public void afterFinallyFindAccountAdvice(JoinPoint theJoinPoint) {
		//print the method for after
		String method = theJoinPoint.getSignature().toShortString();
		myLogger.info("\n=======>>> Executing @AfterFinally on method: " + method);
	}

	
	private void convertAccountNamesToUpperCase(List<Account> result) {
		for(Account account : result) {
			account.setName(account.getName().toUpperCase());
		}
	}


	@Before("com.aop.projetone.aspect.TheAopExpressions.forDaoPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint theJoinPoint) {
		myLogger.info("\n ====>>>> Code first @before the account method");
		
		MethodSignature methode = (MethodSignature) theJoinPoint.getSignature();
		
		myLogger.info("Method : " + methode);
		
		//take the arguments
		Object[] args = theJoinPoint.getArgs();
		myLogger.info(args.toString());
		
		//loop
		
		for(Object tempArg : args) {
			myLogger.info(tempArg.toString());
			
			if(tempArg instanceof Account) {
			Account theAccount = (Account) tempArg;
			myLogger.info("account name : " + theAccount.getName());
			}
		}
		
	}
	
	
}
